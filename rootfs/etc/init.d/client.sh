#/bin/bash

#################################################################################
# The MIT License (MIT)                                                         #
#                                                                               #
# Copyright (c) 2013, Aaron Herting                                             #
#                                                                               #
# Permission is hereby granted, free of charge, to any person obtaining a copy  #
# of this software and associated documentation files (the "Software"), to deal #
# in the Software without restriction, including without limitation the rights  #
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell     #
# copies of the Software, and to permit persons to whom the Software is         #
# furnished to do so, subject to the following conditions:                      #
#                                                                               #
# The above copyright notice and this permission notice shall be included in    #
# all copies or substantial portions of the Software.                           #
#                                                                               #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR    #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,      #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE   #
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER        #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, #
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN     #
# THE SOFTWARE.                                                                 #
#################################################################################

# Set default values
DEBUG='false'

# Default hostname if none is received
hostname='slitaz'

# Set color escape codes
GREEN='\e[1;32m'
RED='\e[0;31m'
CYAN='\e[0;36m'
NC='\e[0m'
YELLOW='\e[1;33m'


# Clear the screen
clear


# Parse options set in /proc/cmdline
echo -e "${CYAN}Parse options...${NC}"
for option in `cat /proc/cmdline`; do 
	key=`echo $option | cut -d= -f1`
	value=`echo $option | cut -d= -f2`
	echo -n 'key: '
	echo -n "$key"
	echo -ne "\t"
	echo -n 'value: '
	echo $value

	if [[ $key == 'bt-hostname' ]] ; then
		hostname=$value
	fi

	if [[ $key == 'bt-torrent' ]] ; then
		echo -n "$value" > /tmp/torrent.url
	fi

	if [[ $key == 'bt-ctcs' ]] ; then
		echo -n "$value" > /tmp/ctcs.srv
	fi

	if [[ $key == 'bt-drive' ]] ; then
		echo -n "$value" > /tmp/drive.path
	fi

	if [[ $key == 'bt-debug' ]] ; then
	  DEBUG='true'
	fi  

done
echo -e "${CYAN}Parse options... [ ${GREEN}DONE${CYAN} ]${NC}"

# Save hostname. Used for CTCS info.
echo -e "${CYAN}Saving hostname...${NC}"
echo -n "$hostname" > /tmp/hostname.txt
echo -e "${CYAN}Saving hostname... [ ${GREEN}DONE${CYAN} ]${NC}"


# Confirm we actually can talk to the internet. Will wait about
# 20 seconds for a network connection then will fail.
echo -e "${CYAN}Confirming network...${NC}"
count=0
while ! ( ping -c 2 -W 1 8.8.8.8 > /dev/null ) ; do
	if ( ps ax | grep udhcpc | grep -v grep > /dev/null ) ; then
		sleep 5
	else
		udhcpc eth0
	fi
	count=`echo $count | awk '{ print $1 + 1 ; }'`
	if [[ $count == '20' ]] ; then 
		echo -e "${CYAN}Confirming network... [ ${RED}FAIL${CYAN} ]${NC}"
		exit 1
	fi
done
echo -e "${CYAN}Confirming network... [ ${GREEN}DONE${CYAN} ]${NC}"


# Wget the torrent file from the url set in options
echo -e "${CYAN}Pulling torrent file...${NC}"
if [[ -f /tmp/torrent.url ]] ; then
	wget `cat /tmp/torrent.url` -O - > /tmp/$hostname
else
	echo -e "${CYAN}Pulling torrent file... [ ${GREEN}FAIL${CYAN} ]${NC}"
	exit 1
fi
echo -e "${CYAN}Pulling torrent file... [ ${GREEN}DONE${CYAN} ]${NC}"


# Set Ctorrent options
CTORRENT_OPTIONS=" -p 51413 -S `cat /tmp/ctcs.srv` -s `cat /tmp/drive.path` -r /tmp/`cat /tmp/hostname.txt` -A cTorrent-image_client/dnh3.3.2-patched -C 256"


# Begin the download
echo -e "${CYAN}Downloading and seeding...${NC}"
/usr/local/bin/ctorrent $CTORRENT_OPTIONS
echo -e "${RED}IT SHOULD BE RARE THAT YOU SEE ME... MAYBE SOMETHING WENT WRONG?${NC}"


